const http = require("http");
const path = require("path");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");

const port = process.env.port || 8000;

const server = http.createServer((req, res) => {
  res.write("<h1>Welcome Boys !</h1>");
  // res.end();
  console.log(req.url);
  if (req.url === "/html") {
    res.write(`<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>
        
          </body>
        </html>`);
    res.end();
  } else if (req.url === "/json") {
    // res.setHeader('Content-Type','application/json' )
    res.write(
      JSON.stringify({
        slideshow: {
          author: "Yours Truly",
          date: "date of publication",
          slides: [
            {
              title: "Wake up to WonderWidgets!",
              type: "all",
            },
            {
              items: [
                "Why <em>WonderWidgets</em> are great",
                "Who <em>buys</em> WonderWidgets",
              ],
              title: "Overview",
              type: "all",
            },
          ],
          title: "Sample Slide Show",
        },
      })
    );
  } else if (req.url === "/uuid") {
    res.end(JSON.stringify({ uuid: uuidv4() }));
  }
  else if(req.url.startsWith('/status/'))
  {
    const statuscode=parseInt(req.url.split('/')[2]);
    res.statusCode=statuscode;
    res.end(`Response with status code ${statuscode}`)
  }

  else if(req.url.startsWith('/delay/'))
  {
    const delayinseconds=parseInt(req.url.split('/')[2]);

    setTimeout(() => {
        res.statusCode=200;
        res.end(`<h4>Response after delay of ${delayinseconds} seconds</h4>`);
    }, delayinseconds  *1000);
  }

  else{
    res.statusCode=404;
    res.end("Page not Found")
  }
});

server.listen(port, () => {
  console.log(`Server is started on ${port}`);
});
